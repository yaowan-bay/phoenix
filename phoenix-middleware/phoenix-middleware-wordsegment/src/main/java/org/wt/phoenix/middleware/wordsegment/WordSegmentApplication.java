package org.wt.phoenix.middleware.wordsegment;

import org.apdplat.word.WordSegmenter;
import org.apdplat.word.segmentation.Word;
import org.apdplat.word.tagging.SynonymTagging;

import java.util.Arrays;
import java.util.List;

public class WordSegmentApplication {
    public static void main(String[] args) {
        //移除停用词
        List<Word> words = WordSegmenter.seg("杨尚川是APDPlat应用级产品开发平台的作者");
        List<Word> words2 = WordSegmenter.segWithStopWords("杨尚川是APDPlat应用级产品开发平台的作者");
        Word text = new Word("测试");
        List<Word> words3 = Arrays.asList(text);
        List<Word> words4 = WordSegmenter.segWithStopWords("楚离陌千方百计为无情找回记忆");
        SynonymTagging.process(words);
        SynonymTagging.process(words2);
        SynonymTagging.process(words3);
        SynonymTagging.process(words4);

        words.addAll(words2);
        words.addAll(words3);
        words.addAll(words4);

        for (Word word : words) {
            System.out.println(word.getSynonym());
        }

    }
}

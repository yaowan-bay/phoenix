package org.wt.phoenix.middleware.groovy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 工程启动类
 * @author wanwan
 * @data 2021/8/13 21:36
 */
@SpringBootApplication
public class GroovySandboxApplication {
    public static void main(String[] args) {
        SpringApplication.run(GroovySandboxApplication.class, args);
    }
}

package org.wt.phoenix.middleware.groovy.service;

import org.springframework.stereotype.Service;

@Service
public class ActivityService {
    /**
     * 执行输出日志
     * @param activityName  活动名称
     * @return  标识
     */
    public Boolean execute(String activityName){
        System.out.println("Spring容器中的Service, 活动名称:" + activityName);
        return true;
    }
}

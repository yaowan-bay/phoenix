package org.wt.phoenix.middleware.groovy.script;

import groovy.lang.GroovyShell;
import groovy.lang.Script;
import org.junit.Test;
import org.wt.phoenix.middleware.groovy.GroovySandboxBaseTest;

import java.util.HashMap;
import java.util.Map;

public class ScriptTest extends GroovySandboxBaseTest {

    /**
     * 测试内存泄露场景
     */
    @Test
    public void testMemoryLeak(){
        for (int i = 0; i < 10000000; i++) {
            //创建GroovyShell
            GroovyShell groovyShell = new GroovyShell();
            //装载解析脚本代码
            Script script = groovyShell.parse("\n" +
                    "\n" +
                    "def HelloWorld(){\n" +
                    "    println \"hello world\"\n" +
                    "}");
            //执行
            script.invokeMethod("HelloWorld", null);
        }
    }

    /**
     * 测试正常场景
     */
    @Test
    public void testNormalScene(){
        //创建GroovyShell
        GroovyShell groovyShell = new GroovyShell();
        //装载解析脚本代码
        Script script = groovyShell.parse("\n" +
                "\n" +
                "def HelloWorld(){\n" +
                "    println \"hello world\"\n" +
                "}");

        for (int i = 0; i < 10000000; i++) {
            //执行
            script.invokeMethod("HelloWorld", null);
        }
    }

    @Test
    public void testParamPassing(){
        //创建GroovyShell
        GroovyShell groovyShell = new GroovyShell();
        //装载解析脚本代码
        Script script = groovyShell.parse(
                "/**\n" +
                " * 简易加法\n" +
                " * @param a 数字a\n" +
                " * @param b 数字b\n" +
                " * @return 和\n" +
                " */\n" +
                "def add(int a, int b) {\n" +
                "    return a + b\n" +
                "}\n" +
                "\n" +
                "/**\n" +
                " * map转化为String\n" +
                " * @param paramMap 参数map\n" +
                " * @return 字符串\n" +
                " */\n" +
                "def mapToString(Map<String, String> paramMap) {\n" +
                "    StringBuilder stringBuilder = new StringBuilder();\n" +
                "    paramMap.forEach({ key, value ->\n" +
                "        stringBuilder.append(\"key:\" + key + \";value:\" + value)\n" +
                "    })\n" +
                "    return stringBuilder.toString()\n" +
                "}");

        //执行加法脚本
        Object[] addParam = new Object[]{1, 2};
        int sum = (int) script.invokeMethod("add", addParam);
        System.out.println("a加b的和为:" + sum);

        //执行转换脚本
        Map<String, String> map = new HashMap<>();
        map.put("科目1", "语文");
        map.put("科目2", "数学");
        Object[] mapParam = new Object[]{map};
        String result = (String) script.invokeMethod("mapToString", mapParam);
        System.out.println("mapToString:" + result);
    }
}

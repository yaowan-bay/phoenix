##踩坑记录
1、集成shardingSphere时，报错:Failed to determine a suitable driver class  
原因为：`数据源的自动配置类DruidDataSourceAutoConfigure 在加载spring.datasource或者spring.datasource.druid下的配置，但是我们没有配置。从而导致报出异常Failed to determine a suitable driver class`  
解决方案：`去除依赖com.alibaba:druid-spring-boot-starter,使用原生Druid包，com.alibaba:druid`

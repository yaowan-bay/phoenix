package org.wt.phoenix.middleware.shardingsphere.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.wt.phoenix.middleware.shardingsphere.domain.ResourceDO;

/**
 * 资源DAO
 * @author wanwan
 * @data 2021/8/13 23:28
 */
@Mapper
public interface ResourceDAO {
    /**
     * 根据id查询券表
     * @param id
     * @return
     */
    ResourceDO findById(@Param("id") Long id);

    /**
     * 根据wid 和 id查询
     * wid为分表key
     * @param wid
     * @param resourceId
     * @return
     */
    ResourceDO findByWidAndResourceId(@Param("wid") Long wid, @Param("resourceId") Long resourceId);

    /**
     * 插入一条数据
     * @param resourceDO
     * @return
     */
    Integer insert(ResourceDO resourceDO);
}

package org.wt.phoenix.middleware.shardingsphere;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.wt.phoenix.middleware.shardingsphere.mockroute.annotation.EnableMockRoute;
import org.wt.phoenix.middleware.shardingsphere.mockroute.domain.RouteResult;
import org.wt.phoenix.middleware.shardingsphere.mockroute.route.MockRouteEngine;

/**
 * springboot集成ShardingSphere沙箱环境启动类
 * @author LTT
 * @date 2021-08-14
 */
@SpringBootApplication
@MapperScan("org.wt.phoenix.middleware.shardingsphere.dao")
@EnableMockRoute
public class ShardingSphereSandboxApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(ShardingSphereSandboxApplication.class, args);
        MockRouteEngine bean = run.getBean(MockRouteEngine.class);
        RouteResult route = bean.route("resource", 1, "wid");
        System.out.println(route);
    }
}

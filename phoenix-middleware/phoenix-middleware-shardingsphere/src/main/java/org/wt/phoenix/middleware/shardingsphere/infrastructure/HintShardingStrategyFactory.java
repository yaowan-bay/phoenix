package org.wt.phoenix.middleware.shardingsphere.infrastructure;

import org.apache.shardingsphere.api.config.sharding.strategy.HintShardingStrategyConfiguration;
import org.apache.shardingsphere.core.strategy.route.ShardingStrategy;
import org.apache.shardingsphere.core.strategy.route.ShardingStrategyFactory;

public class HintShardingStrategyFactory {
    private volatile static ShardingStrategy hintShardingStrategy;

    public static ShardingStrategy getInstance(){
        if (hintShardingStrategy == null){
            synchronized (HintShardingStrategyFactory.class){
                if (hintShardingStrategy == null){
                    hintShardingStrategy = ShardingStrategyFactory.newInstance(new HintShardingStrategyConfiguration(new Hint()));
                }
            }
        }
        return hintShardingStrategy;
    }
}

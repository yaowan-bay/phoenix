package org.wt.phoenix.middleware.shardingsphere.infrastructure;

import org.apache.shardingsphere.api.sharding.hint.HintShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.hint.HintShardingValue;

import java.util.ArrayList;
import java.util.Collection;

public class Hint implements HintShardingAlgorithm {
    @Override
    public Collection<String> doSharding(Collection collection, HintShardingValue hintShardingValue) {
        Integer index = (Integer) hintShardingValue.getValues().iterator().next();
        Object target = new ArrayList<>(collection).get(index);
        ArrayList result = new ArrayList<>();
        result.add(target);
        return result;
    }
}

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.apache.shardingsphere.api.hint;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import lombok.Generated;

import java.util.Collection;
import java.util.Collections;

public final class HintManager implements AutoCloseable {
    public static final ThreadLocal<HintManager> HINT_MANAGER_HOLDER = new ThreadLocal();
    public final Multimap<String, Comparable<?>> databaseShardingValues = HashMultimap.create();
    public final Multimap<String, Comparable<?>> tableShardingValues = HashMultimap.create();
    public boolean databaseShardingOnly;
    public boolean masterRouteOnly;

    public static HintManager getInstance() {
        Preconditions.checkState(null == HINT_MANAGER_HOLDER.get(), "Hint has previous value, please clear first.");
        HintManager result = new HintManager();
        HINT_MANAGER_HOLDER.set(result);
        return result;
    }

    public void setDatabaseShardingValue(Comparable<?> value) {
        this.databaseShardingValues.clear();
        this.tableShardingValues.clear();
        this.databaseShardingValues.put("", value);
        this.databaseShardingOnly = true;
    }

    public void addDatabaseShardingValue(String logicTable, Comparable<?> value) {
        if (this.databaseShardingOnly) {
            this.databaseShardingValues.removeAll("");
        }

        this.databaseShardingValues.put(logicTable, value);
        this.databaseShardingOnly = false;
    }

    public void addTableShardingValue(String logicTable, Comparable<?> value) {
        if (this.databaseShardingOnly) {
            this.databaseShardingValues.removeAll("");
        }

        this.tableShardingValues.put(logicTable, value);
        this.databaseShardingOnly = false;
    }

    public static Collection<Comparable<?>> getDatabaseShardingValues() {
        return getDatabaseShardingValues("");
    }

    public static Collection<Comparable<?>> getDatabaseShardingValues(String logicTable) {
        return (Collection)(null == HINT_MANAGER_HOLDER.get() ? Collections.emptyList() : ((HintManager)HINT_MANAGER_HOLDER.get()).databaseShardingValues.get(logicTable));
    }

    public static Collection<Comparable<?>> getTableShardingValues(String logicTable) {
        return (Collection)(null == HINT_MANAGER_HOLDER.get() ? Collections.emptyList() : ((HintManager)HINT_MANAGER_HOLDER.get()).tableShardingValues.get(logicTable));
    }

    public static boolean isDatabaseShardingOnly() {
        return null != HINT_MANAGER_HOLDER.get() && ((HintManager)HINT_MANAGER_HOLDER.get()).databaseShardingOnly;
    }

    public void setMasterRouteOnly() {
        this.masterRouteOnly = true;
    }

    public static boolean isMasterRouteOnly() {
        return null != HINT_MANAGER_HOLDER.get() && ((HintManager)HINT_MANAGER_HOLDER.get()).masterRouteOnly;
    }

    public static void clear() {
        HINT_MANAGER_HOLDER.remove();
    }

    public void close() {
        clear();
    }

    @Generated
    private HintManager() {
    }
}

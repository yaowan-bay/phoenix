package org.wt.phoenix.middleware.test.shardingsphere;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.wt.phoenix.middleware.shardingsphere.ShardingSphereSandboxApplication;


/**
 * @author wanwan
 * @data 2021/8/14 14:18
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ShardingSphereSandboxApplication.class)
public class ShardingSphereSandboxBaseTest {

}

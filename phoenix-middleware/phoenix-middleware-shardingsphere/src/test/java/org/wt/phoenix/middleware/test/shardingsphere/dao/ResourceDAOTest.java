package org.wt.phoenix.middleware.test.shardingsphere.dao;

import org.apache.shardingsphere.api.hint.HintManager;
import org.junit.Test;
import org.wt.phoenix.middleware.shardingsphere.dao.ResourceDAO;
import org.wt.phoenix.middleware.shardingsphere.domain.ResourceDO;
import org.wt.phoenix.middleware.test.shardingsphere.ShardingSphereSandboxBaseTest;

import javax.annotation.Resource;

/**
 * @author wanwan
 * @data 2021/8/14 14:25
 */
public class ResourceDAOTest extends ShardingSphereSandboxBaseTest {
    @Resource
    private ResourceDAO resourceDAO;

    @Test
    public void testFindById(){
        HintManager hintManager = HintManager.getInstance();
        hintManager.addDatabaseShardingValue("resource",1);
        hintManager.addTableShardingValue("resource",1);

        ResourceDO resourceDO = resourceDAO.findById(1l);
        System.out.println(resourceDO);
    }

    @Test
    public void testFindByWidAndResourceId(){
        ResourceDO resourceDO = resourceDAO.findByWidAndResourceId(1l,1l);
        System.out.println(resourceDO);
    }

    @Test
    public void testInsert100(){
        long startTime = System.currentTimeMillis();
        for (long i = 0; i < 100; i++) {
            ResourceDO resourceDO = new ResourceDO();
            resourceDO.setCode("001");
            resourceDO.setResourceTemplateId(1l);
            resourceDO.setQrCode("001");
            resourceDO.setWid(i);
            Integer insertResult = resourceDAO.insert(resourceDO);
            System.out.println(insertResult);
        }
        System.out.println("插入100条耗时：" + (System.currentTimeMillis() - startTime));
    }

}

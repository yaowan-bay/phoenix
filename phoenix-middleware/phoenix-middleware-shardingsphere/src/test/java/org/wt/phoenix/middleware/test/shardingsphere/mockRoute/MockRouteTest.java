package org.wt.phoenix.middleware.test.shardingsphere.mockRoute;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.wt.phoenix.middleware.shardingsphere.mockroute.domain.RouteResult;
import org.wt.phoenix.middleware.shardingsphere.mockroute.route.MockRouteEngine;
import org.wt.phoenix.middleware.test.shardingsphere.ShardingSphereSandboxBaseTest;

public class MockRouteTest extends ShardingSphereSandboxBaseTest {
    @Autowired
    MockRouteEngine mockRouteEngine;

    @Test
    public void testMockRoute(){
        for (int i = 0; i < 10; i++) {
            RouteResult result = mockRouteEngine.route("resource", i, "wid");
            System.out.println("值:" + i + "," + result);
        }
    }
}

package org.wt.phoenix.middleware.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot集成mybatis沙箱环境启动类
 * @author wanwan
 * @data 2021/8/13 22:44
 */
@SpringBootApplication
@MapperScan("org.wt.phoenix.middleware.mybatis.dao")
public class MybatisSandboxApplication {
    public static void main(String[] args) {
        SpringApplication.run(MybatisSandboxApplication.class, args);
    }
}

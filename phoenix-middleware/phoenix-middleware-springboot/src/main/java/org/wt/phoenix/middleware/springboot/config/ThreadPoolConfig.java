package org.wt.phoenix.middleware.springboot.config;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
public class ThreadPoolConfig {

    @Bean("myAsyncThreadPool")
    public ThreadPoolExecutor asyncThreadPool(){
        return new ThreadPoolExecutor(5, 10, 60, TimeUnit.MILLISECONDS, new ArrayBlockingQueue(100)
                ,new ThreadFactoryBuilder().setNameFormat("demo-pool-%d").build(), new ThreadPoolExecutor.AbortPolicy());
    }
}

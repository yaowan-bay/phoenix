package org.wt.phoenix.middleware.springboot.retry;

/**
 * 重试服务
 */
public interface RetryService {

    /**
     * 尝试执行某任务
     * @param taskName
     * @return
     */
    String tryExecute(String taskName);
}

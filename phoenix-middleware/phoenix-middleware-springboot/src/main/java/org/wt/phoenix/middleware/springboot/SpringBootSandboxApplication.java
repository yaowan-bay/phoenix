package org.wt.phoenix.middleware.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 工程启动类
 * @author wanwan
 * @data 2021/8/13 21:36
 */
@SpringBootApplication
@EnableAsync
@EnableRetry
public class SpringBootSandboxApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootSandboxApplication.class, args);
    }
}

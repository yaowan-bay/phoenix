package org.wt.phoenix.middleware.springboot.async;

public interface AsyncService {

    /**
     * 异步执行任务
     * @param taskName 任务名称
     */
    void asyncExecuteTask(String taskName);
}

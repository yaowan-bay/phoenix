package org.wt.phoenix.middleware.springboot.async.impl;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.wt.phoenix.middleware.springboot.async.AsyncService;
import org.wt.phoenix.middleware.springboot.retry.RetryService;

import javax.annotation.Resource;

@Service
public class AsyncServiceImpl implements AsyncService {
    @Resource
    private RetryService retryService;

    /**
     * 异步执行任务，至于Async注解中的自定义线程池，详见{@link org.wt.phoenix.middleware.springboot.config.ThreadPoolConfig}
     * @param taskName 任务名称
     */
    @Async("myAsyncThreadPool")
    @Override
    public void asyncExecuteTask(String taskName) {
        System.out.println("当前线程名称：" + Thread.currentThread().getName() + ", 执行了任务：" + taskName);
        retryService.tryExecute(taskName);
    }
}

package org.wt.phoenix.middleware.springboot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * controller案例，用于测试Spring容器启动是否成功
 * @author wanwan
 * @data 2021/8/13 21:36
 */
@RestController
public class DemoController {

    @GetMapping("/hello")
    public String hello(){
        return "Hello,Phoenix";
    }
}

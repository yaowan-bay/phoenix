package org.wt.phoenix.middleware.test.springboot;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.wt.phoenix.middleware.springboot.SpringBootSandboxApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SpringBootSandboxApplication.class)
public class SpringBootSandboxBaseTest {
}

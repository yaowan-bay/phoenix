package org.wt.phoenix.middleware.test.springboot.async;

import org.junit.Test;
import org.wt.phoenix.middleware.springboot.async.AsyncService;
import org.wt.phoenix.middleware.test.springboot.SpringBootSandboxBaseTest;

import javax.annotation.Resource;

public class AsyncServiceTest extends SpringBootSandboxBaseTest {
    @Resource
    private AsyncService asyncService;

    @Test
    public void testAsyncExecuteTask(){
        System.out.println("主线程：" + Thread.currentThread().getName());
        asyncService.asyncExecuteTask("测试任务");
    }
}

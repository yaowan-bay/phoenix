package org.wt.phoenix.middleware.test.springboot.retry;

import org.junit.Test;
import org.springframework.retry.RetryException;
import org.wt.phoenix.middleware.springboot.retry.RetryService;
import org.wt.phoenix.middleware.test.springboot.SpringBootSandboxBaseTest;

import javax.annotation.Resource;

public class RetryServiceTest extends SpringBootSandboxBaseTest {
    @Resource
    private RetryService retryService;

    @Test
    public void testTryExecute(){
        Boolean isException = false;
        Integer index = 1;
        while (!isException) {
            try {
                retryService.tryExecute("测试任务" + index++);
            } catch (RetryException e) {
                isException = true;
            }
        }
    }
}

package org.wt.phoenix.middleware.shardingsphere.mockroute.conf;

import org.apache.shardingsphere.shardingjdbc.spring.boot.sharding.SpringBootShardingRuleConfigurationProperties;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter(SpringBootShardingRuleConfigurationProperties.class)
@ComponentScan(basePackages = {"org.wt.phoenix.middleware.shardingsphere.mockroute"})
public class AutoMockRouteConfig {
}

package org.wt.phoenix.middleware.shardingsphere.mockroute.annotation;

import org.springframework.context.annotation.Import;
import org.wt.phoenix.middleware.shardingsphere.mockroute.conf.AutoMockRouteConfig;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自动装配注解
 * 主要采用@Import注解实现在装配此注解时，自动注入对应的config类
 * 
 * @author wanwan
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(AutoMockRouteConfig.class)
public @interface EnableMockRoute {
}

package org.wt.phoenix.middleware.shardingsphere.mockroute.route;

import org.apache.shardingsphere.core.yaml.config.sharding.YamlTableRuleConfiguration;
import org.apache.shardingsphere.core.yaml.config.sharding.strategy.YamlBaseShardingStrategyConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.wt.phoenix.middleware.shardingsphere.mockroute.domain.RouteResult;
import org.wt.phoenix.middleware.shardingsphere.mockroute.route.resolve.ShardingConfigResolve;


@Component
public class MockRouteEngine {
    @Autowired
    ShardingConfigResolve shardingConfigResolve;

    @Autowired
    DatabaseRouteEngine databaseRouteEngine;

    @Autowired
    TableRouteEngine tableRouteEngine;

    public RouteResult route(String logicTable, Object shardingValue, String column){
        YamlTableRuleConfiguration ruleConfiguration = shardingConfigResolve.obtainRuleStrategy(logicTable);
        YamlBaseShardingStrategyConfiguration databaseStrategy = shardingConfigResolve.obtainDatabaseStrategy(logicTable);
        String databaseIndex = databaseRouteEngine.route(databaseStrategy, shardingValue, column, ruleConfiguration);

        YamlBaseShardingStrategyConfiguration tableStrategy = shardingConfigResolve.obtainTableStrategy(logicTable);
        String tableIndex = tableRouteEngine.route(tableStrategy, shardingValue, column, ruleConfiguration);

        return new RouteResult().setLogicTable(logicTable).setDatabaseIndex(databaseIndex).setTableIndex(tableIndex);
    }
}

package org.wt.phoenix.middleware.shardingsphere.mockroute.route.resolve;

import org.apache.shardingsphere.core.yaml.config.sharding.YamlTableRuleConfiguration;
import org.apache.shardingsphere.core.yaml.config.sharding.strategy.YamlBaseShardingStrategyConfiguration;

/**
 * sharding配置文件解析接口
 * @author wanwan
 */
public interface ShardingConfigResolve {
    /**
     * 根据逻辑表名称获取分表规则
     * @param logicTable
     * @return
     */
    YamlBaseShardingStrategyConfiguration obtainTableStrategy(String logicTable);

    /**
     * 根据逻辑表名称获取分库规则
     * @param logicTable
     * @return
     */
    YamlBaseShardingStrategyConfiguration obtainDatabaseStrategy(String logicTable);

    /**
     * 根据逻辑表名称获取表规则
     * @param logicTable
     * @return
     */
    YamlTableRuleConfiguration obtainRuleStrategy(String logicTable);
}

package org.wt.phoenix.middleware.shardingsphere.mockroute.route.resolve;

import org.apache.shardingsphere.core.yaml.config.sharding.YamlShardingStrategyConfiguration;
import org.apache.shardingsphere.core.yaml.config.sharding.YamlTableRuleConfiguration;
import org.apache.shardingsphere.core.yaml.config.sharding.strategy.YamlBaseShardingStrategyConfiguration;
import org.apache.shardingsphere.shardingjdbc.spring.boot.sharding.SpringBootShardingRuleConfigurationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class ShardingConfigDefaultResolve implements ShardingConfigResolve {
    @Autowired
    private SpringBootShardingRuleConfigurationProperties shardingConfig;

    @Override
    public YamlBaseShardingStrategyConfiguration obtainTableStrategy(String logicTable) {
        return this.getRealStrategy(this.obtainRuleStrategy(logicTable).getTableStrategy());
    }

    @Override
    public YamlBaseShardingStrategyConfiguration obtainDatabaseStrategy(String logicTable) {
        return this.getRealStrategy(this.obtainRuleStrategy(logicTable).getDatabaseStrategy());
    }

    @Override
    public YamlTableRuleConfiguration obtainRuleStrategy(String logicTable) {
        return this.shardingConfig.getTables().get(logicTable);
    }

    public YamlBaseShardingStrategyConfiguration getRealStrategy(YamlShardingStrategyConfiguration strategyConfiguration){
        if (strategyConfiguration.getComplex() != null){
            return strategyConfiguration.getComplex();
        }
        if (strategyConfiguration.getHint() != null){
            return strategyConfiguration.getHint();
        }
        if (strategyConfiguration.getInline() != null){
            return strategyConfiguration.getInline();
        }
        if (strategyConfiguration.getStandard() != null) {
            return strategyConfiguration.getStandard();
        }
        if (strategyConfiguration.getNone() != null){
            return strategyConfiguration.getNone();
        }
        return null;
    }
}

package org.wt.phoenix.middleware.shardingsphere.mockroute.route.handler;


import org.wt.phoenix.middleware.shardingsphere.mockroute.domain.HandlerContext;


public interface ShardingStrategyHandler {
    /**
     * 处理分片策略
     * @param strategyConfiguration
     * @param shardingValue
     * @return
     */
    String handler(HandlerContext context);
}

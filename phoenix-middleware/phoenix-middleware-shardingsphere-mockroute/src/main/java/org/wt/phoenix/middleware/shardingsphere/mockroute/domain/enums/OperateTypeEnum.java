package org.wt.phoenix.middleware.shardingsphere.mockroute.domain.enums;

public enum OperateTypeEnum {
    TABLE(1,"分表"),
    DATABASE(2,"分库"),
    ;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 描述
     */
    private String desc;

    OperateTypeEnum(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public OperateTypeEnum setType(Integer type) {
        this.type = type;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public OperateTypeEnum setDesc(String desc) {
        this.desc = desc;
        return this;
    }
}

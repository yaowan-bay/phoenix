package org.wt.phoenix.middleware.shardingsphere.mockroute.route;

import org.apache.shardingsphere.core.yaml.config.sharding.YamlTableRuleConfiguration;
import org.apache.shardingsphere.core.yaml.config.sharding.strategy.YamlBaseShardingStrategyConfiguration;
import org.springframework.stereotype.Component;
import org.wt.phoenix.middleware.shardingsphere.mockroute.domain.HandlerContext;
import org.wt.phoenix.middleware.shardingsphere.mockroute.domain.enums.OperateTypeEnum;
import org.wt.phoenix.middleware.shardingsphere.mockroute.route.handler.ShardingStrategyHandlerFactory;


@Component
public class DatabaseRouteEngine {
    /**
     * DB路由
     * @param databaseStrategy
     * @param shardingValue
     * @return
     */
    public String route(YamlBaseShardingStrategyConfiguration databaseStrategy,
                        Object shardingValue,
                        String column,
                        YamlTableRuleConfiguration ruleConfiguration){
        HandlerContext context = new HandlerContext().setShardingValue(shardingValue).setStrategyConfiguration(databaseStrategy)
                .setColumn(column).setOperateTypeEnum(OperateTypeEnum.DATABASE).setRuleConfiguration(ruleConfiguration);
        return ShardingStrategyHandlerFactory.getHandler(databaseStrategy).handler(context);
    }
}

package org.wt.phoenix.middleware.shardingsphere.mockroute.route.handler;

import org.apache.shardingsphere.core.yaml.config.sharding.strategy.YamlBaseShardingStrategyConfiguration;
import org.apache.shardingsphere.core.yaml.config.sharding.strategy.YamlComplexShardingStrategyConfiguration;
import org.apache.shardingsphere.core.yaml.config.sharding.strategy.YamlInlineShardingStrategyConfiguration;

import java.util.HashMap;
import java.util.Map;

public class ShardingStrategyHandlerFactory {
    private static Map<Class, ShardingStrategyHandler> HANDLER_MAP = new HashMap<>();

    static {
        HANDLER_MAP.put(YamlInlineShardingStrategyConfiguration.class, new InlineShardingStrategyHandler());
        HANDLER_MAP.put(YamlComplexShardingStrategyConfiguration.class, new ComplexShardingStrategyHandler());
    }

    public static ShardingStrategyHandler getHandler(YamlBaseShardingStrategyConfiguration shardingStrategyConfiguration){
        ShardingStrategyHandler shardingStrategyHandler = HANDLER_MAP.get(shardingStrategyConfiguration.getClass());
        if (shardingStrategyHandler == null){
            throw new RuntimeException("不支持的分片策略类型");
        }
        return shardingStrategyHandler;
    }
}

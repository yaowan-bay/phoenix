package org.wt.phoenix.middleware.shardingsphere.mockroute.domain;

public class RouteResult {
    /**
     * 逻辑表名
     */
    private String logicTable;

    /**
     * 表索引
     */
    private String tableIndex;

    /**
     * 数据库索引
     */
    private String databaseIndex;

    public String getLogicTable() {
        return logicTable;
    }

    public RouteResult setLogicTable(String logicTable) {
        this.logicTable = logicTable;
        return this;
    }

    public String getTableIndex() {
        return tableIndex;
    }

    public RouteResult setTableIndex(String tableIndex) {
        this.tableIndex = tableIndex;
        return this;
    }

    public String getDatabaseIndex() {
        return databaseIndex;
    }

    public RouteResult setDatabaseIndex(String databaseIndex) {
        this.databaseIndex = databaseIndex;
        return this;
    }

    @Override
    public String toString() {
        return "逻辑表名：" + logicTable + ",所在数据库为：" + databaseIndex + ",所在表为：" + tableIndex;
    }
}

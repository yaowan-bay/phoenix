//package org.wt.phoenix.middleware.shardingsphere.mockroute.util;
//
//import org.springframework.beans.BeansException;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class SpringContextAware implements ApplicationContextAware {
//    private static ApplicationContext applicationContext;
//
//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        this.applicationContext = applicationContext;
//    }
//
//    public static ApplicationContext getSpringContext(){
//        return applicationContext;
//    }
//}

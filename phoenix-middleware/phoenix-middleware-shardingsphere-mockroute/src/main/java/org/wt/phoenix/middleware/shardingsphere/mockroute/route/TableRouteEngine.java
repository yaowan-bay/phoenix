package org.wt.phoenix.middleware.shardingsphere.mockroute.route;

import org.apache.shardingsphere.core.yaml.config.sharding.YamlTableRuleConfiguration;
import org.apache.shardingsphere.core.yaml.config.sharding.strategy.YamlBaseShardingStrategyConfiguration;
import org.springframework.stereotype.Component;
import org.wt.phoenix.middleware.shardingsphere.mockroute.domain.HandlerContext;
import org.wt.phoenix.middleware.shardingsphere.mockroute.domain.enums.OperateTypeEnum;
import org.wt.phoenix.middleware.shardingsphere.mockroute.route.handler.ShardingStrategyHandlerFactory;


@Component
public class TableRouteEngine {
    public String route(YamlBaseShardingStrategyConfiguration tableSharding,
                        Object shardingValue,
                        String column,
                        YamlTableRuleConfiguration ruleConfiguration){
        HandlerContext context = new HandlerContext().setShardingValue(shardingValue).setStrategyConfiguration(tableSharding)
                .setColumn(column).setOperateTypeEnum(OperateTypeEnum.TABLE).setRuleConfiguration(ruleConfiguration);
        return ShardingStrategyHandlerFactory.getHandler(tableSharding).handler(context);
    }
}

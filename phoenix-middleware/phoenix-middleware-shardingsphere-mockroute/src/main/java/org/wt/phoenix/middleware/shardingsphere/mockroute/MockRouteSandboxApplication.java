package org.wt.phoenix.middleware.shardingsphere.mockroute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.wt.phoenix.middleware.shardingsphere.mockroute.annotation.EnableMockRoute;

@SpringBootApplication
@EnableMockRoute
public class MockRouteSandboxApplication {
    public static void main(String[] args) {
        SpringApplication.run(MockRouteSandboxApplication.class);
    }
}

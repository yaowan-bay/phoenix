package org.wt.phoenix.middleware.shardingsphere.mockroute.domain;

import org.apache.shardingsphere.core.yaml.config.sharding.YamlTableRuleConfiguration;
import org.apache.shardingsphere.core.yaml.config.sharding.strategy.YamlBaseShardingStrategyConfiguration;
import org.wt.phoenix.middleware.shardingsphere.mockroute.domain.enums.OperateTypeEnum;

public class HandlerContext {
    /**
     * 分表策略配置
     */
    private YamlBaseShardingStrategyConfiguration strategyConfiguration;

    /**
     * 分表值
     */
    private Object shardingValue;

    /**
     * 用户传入分表列名称
     */
    private String column;

    /**
     * 操作类型
     */
    private OperateTypeEnum operateTypeEnum;

    /**
     * 分表策略配置
     */
    private YamlTableRuleConfiguration ruleConfiguration;

    public YamlBaseShardingStrategyConfiguration getStrategyConfiguration() {
        return strategyConfiguration;
    }

    public HandlerContext setStrategyConfiguration(YamlBaseShardingStrategyConfiguration strategyConfiguration) {
        this.strategyConfiguration = strategyConfiguration;
        return this;
    }

    public Object getShardingValue() {
        return shardingValue;
    }

    public HandlerContext setShardingValue(Object shardingValue) {
        this.shardingValue = shardingValue;
        return this;
    }

    public String getColumn() {
        return column;
    }

    public HandlerContext setColumn(String column) {
        this.column = column;
        return this;
    }

    public OperateTypeEnum getOperateTypeEnum() {
        return operateTypeEnum;
    }

    public HandlerContext setOperateTypeEnum(OperateTypeEnum operateTypeEnum) {
        this.operateTypeEnum = operateTypeEnum;
        return this;
    }

    public YamlTableRuleConfiguration getRuleConfiguration() {
        return ruleConfiguration;
    }

    public HandlerContext setRuleConfiguration(YamlTableRuleConfiguration ruleConfiguration) {
        this.ruleConfiguration = ruleConfiguration;
        return this;
    }
}

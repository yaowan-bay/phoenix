package org.wt.phoenix.middleware.test.shardingsphere.mockroute;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.wt.phoenix.middleware.shardingsphere.mockroute.MockRouteSandboxApplication;


/**
 * @author wanwan
 * @data 2021/8/14 14:18
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MockRouteSandboxApplication.class)
public class MockRouteSandboxBaseTest {

}

package org.wt.phoenix.middleware.test.shardingsphere.mockroute.engine;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.wt.phoenix.middleware.shardingsphere.mockroute.domain.RouteResult;
import org.wt.phoenix.middleware.shardingsphere.mockroute.route.MockRouteEngine;
import org.wt.phoenix.middleware.test.shardingsphere.mockroute.MockRouteSandboxBaseTest;

public class MockRouteEngineTest extends MockRouteSandboxBaseTest {
    @Autowired
    private MockRouteEngine routeEngine;

    @Test
    public void testInline(){
        for (int i = 0; i < 10; i++) {
            RouteResult result = routeEngine.route("resource", i, "wid");
            System.out.println("值:" + i + "," + result);
        }
    }
}

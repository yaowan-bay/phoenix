package org.wt.phoenix.middleware.test.shardingsphere.mockroute.script;

import groovy.lang.Closure;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import org.apache.shardingsphere.shardingjdbc.spring.boot.sharding.SpringBootShardingRuleConfigurationProperties;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.wt.phoenix.middleware.test.shardingsphere.mockroute.MockRouteSandboxBaseTest;


public class ClosureTest extends MockRouteSandboxBaseTest {
    @Autowired
    SpringBootShardingRuleConfigurationProperties shardingConf;

    @Test
    public void testClosure() {
        GroovyShell SHELL = new GroovyShell();
        Script script = SHELL.parse("{it -> \"ds${wid % 2}\"}");
        Closure closure = (Closure) script.run();
        closure.setResolveStrategy(Closure.DELEGATE_ONLY);
        closure.setProperty("wid", 1);
        String wid = closure.call().toString();
        System.out.println(wid);
    }
}

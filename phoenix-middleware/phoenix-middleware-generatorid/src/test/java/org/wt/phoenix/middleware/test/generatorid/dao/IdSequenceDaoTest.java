package org.wt.phoenix.middleware.test.generatorid.dao;

import org.junit.Test;
import org.wt.phoenix.middleware.generatorid.dao.IdSequenceDAO;
import org.wt.phoenix.middleware.generatorid.domain.IdSequence;
import org.wt.phoenix.middleware.generatorid.domain.ResourceDO;
import org.wt.phoenix.middleware.test.generatorid.GeneratorIdSandboxBaseTest;

import javax.annotation.Resource;

public class IdSequenceDaoTest extends GeneratorIdSandboxBaseTest {

    @Resource
    private IdSequenceDAO idSequenceDAO;

    @Test
    public void testUpdateCurrentMaxId(){
        String businessTag = "resource";
        IdSequence byBusinessTag = idSequenceDAO.findByBusinessTag(businessTag);
        Long maxId = byBusinessTag.getCurrentMaxId();
        System.out.println(maxId);
        Integer integer = idSequenceDAO.updateCurrentMaxId(maxId, businessTag);
        System.out.println(integer);
    }
}

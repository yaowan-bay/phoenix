package org.wt.phoenix.middleware.test.generatorid.dao;

import org.junit.Test;
import org.wt.phoenix.middleware.generatorid.dao.ResourceDAO;
import org.wt.phoenix.middleware.generatorid.domain.ResourceDO;
import org.wt.phoenix.middleware.test.generatorid.GeneratorIdSandboxBaseTest;

import javax.annotation.Resource;

/**
 * @author wanwan
 * @data 2021/8/14 14:25
 */
public class ResourceDAOTest extends GeneratorIdSandboxBaseTest {
    @Resource
    private ResourceDAO resourceDAO;

    @Test
    public void testFindById(){
        ResourceDO resourceDO = resourceDAO.findById(1l);
        System.out.println(resourceDO);
    }

    @Test
    public void testInsertNoParamAnnotation(){
        ResourceDO resourceDO = new ResourceDO();
        resourceDO.setCode("001");
        resourceDO.setResourceTemplateId(1l);
        resourceDO.setQrCode("001");
        resourceDO.setWid(1l);
        Integer insertResult = resourceDAO.insertNoParamAnnotation(resourceDO);
        System.out.println(insertResult);
    }

    @Test
    public void testInsertHaveParamAnnotation(){
        ResourceDO resourceDO = new ResourceDO();
        resourceDO.setCode("001");
        resourceDO.setResourceTemplateId(1l);
        resourceDO.setQrCode("001");
        resourceDO.setWid(1l);
        Integer insertResult = resourceDAO.insertHaveParamAnnotation(resourceDO);
        System.out.println(insertResult);
    }
}

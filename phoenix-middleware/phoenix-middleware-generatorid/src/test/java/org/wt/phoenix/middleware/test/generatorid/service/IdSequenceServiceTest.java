package org.wt.phoenix.middleware.test.generatorid.service;

import org.junit.Test;
import org.wt.phoenix.middleware.generatorid.service.IdSequenceService;
import org.wt.phoenix.middleware.generatorid.util.SpringContextUtil;
import org.wt.phoenix.middleware.test.generatorid.GeneratorIdSandboxBaseTest;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;


public class IdSequenceServiceTest extends GeneratorIdSandboxBaseTest {

    /**
     * 单机测试获取id
     * 模拟100并发进行获取100万次ID。目前QPS10万，并且是直连公网，如果内网环境估计会有质的变化。
     *
     * 服务器配置：
     * 1核1G数据库
     * 性能报告：
     * 100线程-->100W执行--->本地缓存长度1000--->9W的QPS--->成功率100%
     * 100线程-->100W执行--->本地缓存长度10000--->50W的QPS--->成功率100%
     * 1000线程--->1000W执行--->本地缓存长度10000--->65万QPS--->成功率100%
     * @throws InterruptedException
     */
    @Test
    public void testStandAloneNextId() throws InterruptedException {
        ConcurrentLinkedQueue<Long> idQueue = new ConcurrentLinkedQueue<>();

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1000, 1000, 1000, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(1000), new ThreadPoolExecutor.DiscardPolicy());
        CountDownLatch countDownLatch = new CountDownLatch(1000);

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            threadPoolExecutor.execute(new GetIdTask(SpringContextUtil.getBean(IdSequenceService.class), idQueue, countDownLatch, 10000l));
        }
        countDownLatch.await();
        long endTime = System.currentTimeMillis();

        for (Long id : idQueue) {
            if (id % 1000 == 0){
                System.out.println(id);
            }
        }
        Set<Long> idSet = new HashSet<>(idQueue);
        System.out.println("队列长度:" + idSet.size() + "，用时：" + (endTime - startTime) + ", qps：" + (10000000 / (endTime - startTime) * 1000));
        threadPoolExecutor.shutdown();
    }

    /**
     * 集群测试获取id
     * 模拟20台个服务，每个服务10个线程
     * 要模拟测试需要将{@link org.wt.phoenix.middleware.generatorid.service.impl.IdSequenceServiceImpl}中的idCacheQueue设置为非静态的。
     * 然后在此用例中注入不同的queue实例
     */
    @Test
    public void testClusterNextId() throws InterruptedException {
        ConcurrentLinkedQueue<Long> idQueue = new ConcurrentLinkedQueue<>();

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(100, 100, 1000, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(1000), new ThreadPoolExecutor.DiscardPolicy());
        CountDownLatch countDownLatch = new CountDownLatch(200);

        //获取20个服务,每个服务10个线程
        for (int i = 0; i < 20; i++){
            IdSequenceService idSequenceService = SpringContextUtil.getBean(IdSequenceService.class);
            for (int j = 0; j < 10; j++) {
                threadPoolExecutor.execute(new GetIdTask(idSequenceService, idQueue, countDownLatch, 5000l));
            }
        }
        countDownLatch.await();


        for (Long id : idQueue) {
            if (id % 1000 == 0){
                System.out.println(id);
            }
        }
        System.out.println("队列长度:" + idQueue.size());
        threadPoolExecutor.shutdown();
    }
}

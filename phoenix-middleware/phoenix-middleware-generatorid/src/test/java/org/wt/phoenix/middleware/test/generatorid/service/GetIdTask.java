package org.wt.phoenix.middleware.test.generatorid.service;

import org.wt.phoenix.middleware.generatorid.service.IdSequenceService;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;


public class GetIdTask implements Runnable{
    private IdSequenceService idSequenceService;

    private ConcurrentLinkedQueue idQueue;

    private volatile CountDownLatch countDownLatch;

    private Long loopCount;

    public GetIdTask(IdSequenceService idSequenceService, ConcurrentLinkedQueue idQueue, CountDownLatch countDownLatch, Long loopCount) {
        this.idSequenceService = idSequenceService;
        this.idQueue = idQueue;
        this.countDownLatch = countDownLatch;
        this.loopCount = loopCount;
    }

    @Override
    public void run() {
        for (int i = 0; i < loopCount; i++) {
            idQueue.add(idSequenceService.nextId("resource"));
        }
        countDownLatch.countDown();
    }
}

package org.wt.phoenix.middleware.test.generatorid;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.wt.phoenix.middleware.generatorid.GeneratorIdSandBoxApplication;


/**
 * @author wanwan
 * @data 2021/8/14 14:18
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = GeneratorIdSandBoxApplication.class)
public class GeneratorIdSandboxBaseTest {

}

package org.wt.phoenix.middleware.generatorid;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * GeneratorId组件启动测试类，后续去除，改为组件
 * @author wanwan
 * @data 2021/8/13 22:44
 */
@SpringBootApplication
@MapperScan("org.wt.phoenix.middleware.generatorid.dao")
public class GeneratorIdSandBoxApplication {
    public static void main(String[] args) {
        SpringApplication.run(GeneratorIdSandBoxApplication.class, args);
    }
}

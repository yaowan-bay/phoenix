package org.wt.phoenix.middleware.generatorid.dao;

import org.apache.ibatis.annotations.Param;
import org.wt.phoenix.middleware.generatorid.domain.IdSequence;

public interface IdSequenceDAO {
    /**
     * 根据业务标识查询记录
     * @param businessTag
     * @return
     */
    IdSequence findByBusinessTag(@Param("businessTag") String businessTag);

    /**
     * 根据业务标识查询记录并加锁
     * @param businessTag
     * @return
     */
    IdSequence findByBusinessTagForUpdate(@Param("businessTag") String businessTag);

    /**
     * 根据业务标识和当前最大maxId更新当前最大maxId
     * @param maxId
     * @param businessTag
     * @return
     */
    Integer updateCurrentMaxId(@Param("maxId") Long maxId, @Param("businessTag") String businessTag);

}

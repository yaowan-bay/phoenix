package org.wt.phoenix.middleware.generatorid.domain;

import java.io.Serializable;

public class IdSequence implements Serializable {
    private static final long serialVersionUID = 666666L;

    /**
     * id
     */
    private Long id;

    /**
     * 业务标识
     */
    private String businessTag;

    /**
     * 步长
     */
    private Long step;

    /**
     * 当前最大ID
     */
    private Long currentMaxId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessTag() {
        return businessTag;
    }

    public void setBusinessTag(String businessTag) {
        this.businessTag = businessTag;
    }

    public Long getStep() {
        return step;
    }

    public void setStep(Long step) {
        this.step = step;
    }

    public Long getCurrentMaxId() {
        return currentMaxId;
    }

    public void setCurrentMaxId(Long currentMaxId) {
        this.currentMaxId = currentMaxId;
    }
}

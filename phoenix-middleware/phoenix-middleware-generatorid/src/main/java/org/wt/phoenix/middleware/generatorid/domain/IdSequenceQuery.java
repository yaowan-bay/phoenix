package org.wt.phoenix.middleware.generatorid.domain;

public class IdSequenceQuery {
    /**
     * 业务标识
     */
    String businessTag;

    /**
     * 乐观锁重试次数
     */
    Integer retryNumber;

    public String getBusinessTag() {
        return businessTag;
    }

    public IdSequenceQuery setBusinessTag(String businessTag) {
        this.businessTag = businessTag;
        return this;
    }

    public Integer getRetryNumber() {
        return retryNumber;
    }

    public IdSequenceQuery setRetryNumber(Integer retryNumber) {
        this.retryNumber = retryNumber;
        return this;
    }
}

package org.wt.phoenix.middleware.generatorid.domain;

import java.util.Date;

/**
 * resource表DO
 * @author wanwan
 * @data 2021/8/13 23:13
 */
public class ResourceDO {
    /**
     * id
     */
    private Long id;

    /**
     * 券code
     */
    private String code;

    /**
     * 二维码
     */
    private String qrCode;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 券模板id
     */
    private Long resourceTemplateId;

    /**
     * 用户id
     */
    private Long wid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getResourceTemplateId() {
        return resourceTemplateId;
    }

    public void setResourceTemplateId(Long resourceTemplateId) {
        this.resourceTemplateId = resourceTemplateId;
    }

    public Long getWid() {
        return wid;
    }

    public void setWid(Long wid) {
        this.wid = wid;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ResourceDO{");
        sb.append("id=").append(id);
        sb.append(", code='").append(code).append('\'');
        sb.append(", qrCode='").append(qrCode).append('\'');
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", resourceTemplateId=").append(resourceTemplateId);
        sb.append(", wid=").append(wid);
        sb.append('}');
        return sb.toString();
    }
}

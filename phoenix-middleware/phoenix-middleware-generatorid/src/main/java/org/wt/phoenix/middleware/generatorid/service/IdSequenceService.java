package org.wt.phoenix.middleware.generatorid.service;

public interface IdSequenceService {

    /**
     * 根据businessTag获取下一个id
     * @param businessTag
     * @return
     */
    Long nextId(String businessTag);
}

package org.wt.phoenix.middleware.generatorid.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.wt.phoenix.middleware.generatorid.domain.ResourceDO;

/**
 * 资源DAO
 * @author wanwan
 * @data 2021/8/13 23:28
 */
@Mapper
public interface ResourceDAO {
    /**
     * 根据id查询券表
     * @param id
     * @return
     */
    ResourceDO findById(@Param("id") Long id);

    /**
     * 插入一条数据，不加Param注解
     * 对应xml文件中的sql可以直接使用#{字段名称}
     * @param resourceDO
     * @return
     */
    Integer insertNoParamAnnotation(ResourceDO resourceDO);

    /**
     * 插入一条数据，加Param注解
     * 对应xml文件中的sql不可以直接使用#{字段名称}，必须使用#{resourceDO.字段名称}
     * @param resourceDO
     * @return
     */
    Integer insertHaveParamAnnotation(@Param("resourceDO") ResourceDO resourceDO);
}

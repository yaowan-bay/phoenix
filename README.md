##官网
gitee：https://gitee.com/yaowan-bay/phoenix  
语雀：https://phoenix-wt.yuque.com/  
公告：https://phoenix-wt.yuque.com/books/share/f5e903a2-5b62-4b82-982c-42f8c294d56b  
开发计划：https://phoenix-wt.yuque.com/docs/share/94988a71-f106-4042-a97f-109dabed8989
##Phoenix
phoenix是一个学习开源项目，初期会接入互联网开发技术常用框架及中间件，并提供专门的沙箱测试环境，用于验证中间件原理以及知识。
后续会将各中间件串联起来做集成，以及针对互联网工程常见的坑以及问题，提供解决方案实现Demo。

##项目结构

> phoenix
>> phoenix-middleware  ***提供各种主流中间件的沙箱测试***
>>> phoenix-middleware-springboot  ***提供SpringBoot相关能力的沙箱测试***  
>>> phoenix-middleware-mybatis   ***提供Mybatis相关能力的沙箱测试***  
>>> phoenix-middleware-shardingsphere  ***提供ShardingSphere相关能力的沙箱测试，并提供分布式ID等解决方案***

##项目计划
>2021-08-13：项目建立，初步设计工程结构，接入基础springboot环境

后续待完善...